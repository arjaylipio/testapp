package com.goldendevs.testapplication.common


// Base url of the retrofit
const val API_URL = "https://itunes.apple.com/"

// Db name of Room
const val DATABASE_NAME = "DBMovies"

// Tag for passing Data in the intent
const val MOVIE_DETAILS = "MOVIE_DETAIL_EXTRA"
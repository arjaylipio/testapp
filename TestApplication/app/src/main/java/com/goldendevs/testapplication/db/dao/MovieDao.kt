package com.goldendevs.testapplication.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.goldendevs.testapplication.model.MovieDetailsEntity

@Dao
interface MovieDao {

    @Query("SELECT * FROM movie_details LIMIT 50")
    fun getMovieList(): LiveData<MutableList<MovieDetailsEntity>>

    @Insert
    fun save(movieDetails: MovieDetailsEntity)

    @Update
    fun update(movieDetails: MovieDetailsEntity)

    @Query("DELETE FROM movie_details")
    fun clearAll()
}
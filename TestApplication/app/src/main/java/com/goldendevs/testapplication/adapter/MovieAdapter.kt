package com.goldendevs.testapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.goldendevs.testapplication.R
import com.goldendevs.testapplication.model.MovieDetailsEntity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.listview_item.view.*


class MovieAdapter(private val routes: MutableList<MovieDetailsEntity>) :
    RecyclerView.Adapter<MovieAdapter.ViewHolder>() {

    private var selectRouteCallback: OnSelectRouteCallback? = null
    private var selectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.listview_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = routes.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(routes[position])
    }

    fun updateData(newDataSet: List<MovieDetailsEntity>) {
        routes.clear()
        routes.addAll(newDataSet)
        notifyDataSetChanged()
    }

    fun addData(newDataSet: List<MovieDetailsEntity>){
        routes.addAll(newDataSet)
        notifyDataSetChanged()
    }

    fun setSelectRouteCallback(callback: OnSelectRouteCallback) {
        selectRouteCallback = callback
    }

    fun getSelectedRoute(): MovieDetailsEntity? {
        if (selectedPosition < 0) return null
        return routes[selectedPosition]
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val rootContainer: LinearLayout = itemView.rootContainer
        private val trackName: TextView = itemView.tvTrackName
        private val imgPreview: ImageView = itemView.img_preview
        private val genre: TextView = itemView.tv_genre
        private val price: TextView = itemView.tv_price
        fun bind(movieDetailsEntity: MovieDetailsEntity) {


            trackName.text = movieDetailsEntity.trackName ?: "No Name"
            genre.text = movieDetailsEntity.primaryGenreName ?: "No Genre"
            var p = "P."
            movieDetailsEntity.collectionPrice?.let { p += it.toString() }
            price.text = p

             Picasso.get()
              .load(movieDetailsEntity.artworkUrl100)
              .placeholder(R.drawable.ic_launcher_foreground)
              .error(R.drawable.ic_close_black_24dp)
              .fit()
              .into(imgPreview)


            rootContainer.setOnClickListener {
                selectRouteCallback?.onSelect(movieDetailsEntity)
            }
        }

    }

}

interface OnSelectRouteCallback {
    fun onSelect(route: MovieDetailsEntity)
}

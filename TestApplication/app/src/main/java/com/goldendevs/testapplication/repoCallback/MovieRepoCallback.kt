package com.goldendevs.testapplication.repoCallback

import com.goldendevs.testapplication.model.DataResponse

// Call back for the retrofit response
interface MovieRepoCallback {
    interface OnMovieRepoCallback {
        fun onSuccess(dataResponse: DataResponse)
        fun onFail(errMessage: String)
    }
}
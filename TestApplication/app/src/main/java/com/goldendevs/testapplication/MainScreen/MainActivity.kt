package com.goldendevs.testapplication.MainScreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.goldendevs.testapplication.DetailScreen.DetailActivity
import com.goldendevs.testapplication.R
import com.goldendevs.testapplication.adapter.MovieAdapter
import com.goldendevs.testapplication.adapter.OnSelectRouteCallback
import com.goldendevs.testapplication.common.MOVIE_DETAILS
import com.goldendevs.testapplication.model.MovieDetailsEntity
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_nav.view.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var mainActViewModel : MainActivityViewModel
    private lateinit var adapter: MovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Instance of MainActivityViewModel
        mainActViewModel = ViewModelProviders.of(this)[MainActivityViewModel::class.java]

        subscribeUi()
        setupList()
        mainActViewModel.getMovies()

        val date = getCurrentDateTime()
        val dateInString = date.toString("MM/dd/yyyy")
        toolbar.tv_date.text = dateInString
    }

    // Subcribe from Viewmodel livedata
    private fun subscribeUi() {

        mainActViewModel.getDbMovies().observe(this, Observer {
            adapter.updateData(it)
            adapter.setSelectRouteCallback(object :
                OnSelectRouteCallback {
                override fun onSelect(route: MovieDetailsEntity) {
                    Log.d("ROUTE_CALLBACK",route.toString())

                    var intent = Intent(this@MainActivity, DetailActivity::class.java)
                    intent.putExtra(MOVIE_DETAILS, route)
                    startActivity(intent)
                }
            })
        })
    }


    // Get Date Formater
    fun Date.toString(format: String, locale: Locale = Locale.getDefault()): String {
        val formatter = SimpleDateFormat(format, locale)
        return formatter.format(this)
    }

    // Get Date
    fun getCurrentDateTime(): Date {
        return Calendar.getInstance().time
    }

    // Set up Recyclerview
    private fun setupList() {
        val layoutManager = LinearLayoutManager(this)
        rv_moview_list.layoutManager = layoutManager

        rv_moview_list.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL ,false)
        adapter = MovieAdapter(mutableListOf())


        // add AddOnScrollListener
        rv_moview_list.adapter = adapter
    }

}

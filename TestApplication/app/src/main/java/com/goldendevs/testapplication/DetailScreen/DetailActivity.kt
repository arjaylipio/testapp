package com.goldendevs.testapplication.DetailScreen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.goldendevs.testapplication.R
import com.goldendevs.testapplication.common.MOVIE_DETAILS
import com.goldendevs.testapplication.model.MovieDetailsEntity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    lateinit var md : MovieDetailsEntity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        // Get Data from the intent
         md  = intent.getSerializableExtra(MOVIE_DETAILS) as MovieDetailsEntity
        Log.d("DetailActivity", md.toString())
        putDetails()
    }

    // Populate view with the data from the intent
    private fun putDetails() {
        tv_title.text = md.collectionName ?: "No Title"
        var releasedate = "Release Date: \n"
        releasedate += md.releaseDate ?: "No Date Release!"
        tv_release_date.text = releasedate
        tv_summary.text = md.longDescription ?: "No Info!"

        Picasso.get()
            .load(md.artworkUrl100)
            .placeholder(R.drawable.ic_launcher_foreground)
            .error(R.drawable.ic_close_black_24dp)
            .fit()
            .into(img_large_pic)

        Picasso.get()
            .load(md.artworkUrl30)
            .placeholder(R.drawable.ic_launcher_foreground)
            .error(R.drawable.ic_close_black_24dp)
            .fit()
            .into(img_preview_pic)

        img_back.setOnClickListener{
            finish()
        }
    }
}

# README #

Test app for appetiser

Simple app with recycler view and simple api call and local database storage.

# Architecture #

# MVVM Architecture 
 * the Model (the underlying data and domain objects).
 * the View (what the user actually sees and interacts with)
 * the facade that translates between the Model and View. (the ViewModel in this case)

# Dependencies #

# Picasso 
* simple to use
* Handling ImageView recycling and download cancelation in an adapter.
* Complex image transformations with minimal memory use.
* Automatic memory and disk caching.

# Koin 
* written only with Kotlin
* pragmatic lightweight dependency
* much easier than dagger
	
# Retrofit 
* Multipart request body and file upload
* Object conversion to request body (e.g., JSON, protocol buffers)
* URL parameter replacement and query parameter support

# Lifecycle 
* Ensures your UI matches your data state
* No memory leaks
* No more manual lifecycle handling